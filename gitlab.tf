
# Configure the GitLab Provider
provider "gitlab" {
    token = "${var.gitlab_token}"
}

# Add a project owned by the user
resource "gitlab_project" "devops_project" {
    name = "terraform_rebrainme"
    visibility_level = "public"
    description = "DevOps repo"
    default_branch = "master"
}
# Add a deploy key to the project
resource "gitlab_deploy_key" "deploy_key" {
    project = "${gitlab_project.devops_project.id}"
    title = "deploy key with push access"
    key = "${var.ssh_key}"
    can_push = true
}
