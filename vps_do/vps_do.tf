provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_tag" "zaytsev" {
  name = "zaytsev"
}

#resource "digitalocean_ssh_key" "rebrainme" {
#  name       = "Rebrainme key"
#  public_key = "${var.ssh_key_rebrainme}"
#}

resource "digitalocean_ssh_key" "zaytsev" {
  name       = "Zaytsev key"
  public_key = "${var.ssh_key_zaytsev}"
}

resource "digitalocean_droplet" "web" {
  image  = "ubuntu-18-04-x64"
  name   = "rebrainme-1"
  region = "nyc1"
  size   = "s-1vcpu-1gb"
  ssh_keys = ["${digitalocean_ssh_key.zaytsev.id}","23947889"]
  tags   = ["${digitalocean_tag.zaytsev.id}"]
}