provider "vscale" {
  token = "${var.vscale_token}"
}

resource "vscale_ssh_key" "zaytsev" {
  name       = "Zaytsev key"
  key = "${var.ssh_key_zaytsev}"
}

resource "vscale_scalet" "web" {
  name   = "zaytsev_vscale"
  make_from = "centos_7_64_001_master"
  rplan = "small"
  location = "msk0"
  ssh_keys = ["${vscale_ssh_key.zaytsev.id}"]
}

