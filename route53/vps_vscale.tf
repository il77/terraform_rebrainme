provider "vscale" {
  token = "${var.vscale_token}"
}

resource "vscale_ssh_key" "zaytsev" {
  name       = "Zaytsev key"
  key = "${var.ssh_key_zaytsev}"
}
resource "vscale_ssh_key" "rebrainme" {
 name       = "Rebrainme key"
 key = "${var.ssh_key_rebrainme}"
}

resource "vscale_scalet" "route53" {
  name   = "zaytsev_route53"
  make_from = "centos_7_64_001_master"
  rplan = "medium"
  location = "msk0"
  ssh_keys = ["${vscale_ssh_key.zaytsev.id}","${vscale_ssh_key.rebrainme.id}"]
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

resource "aws_route53_record" "www" {
  zone_id = "${var.aws_zone_id}"
  name    = "il77.devops.rebrain.srwx.net"
  type    = "A"
  ttl     = "300"
  records = ["${vscale_scalet.route53.public_address}"]
}
